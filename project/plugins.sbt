logLevel := Level.Warn

resolvers ++= Seq(
  Resolver.typesafeRepo("releases"),
  Resolver.sonatypeRepo("releases")
)

// source code formatting
// https://github.com/daniel-trinh/scalariform
addSbtPlugin("org.scalariform" % "sbt-scalariform" % "1.5.1")

// adds styling rules for scala
// http://www.scalastyle.org/rules-0.7.0.html
addSbtPlugin("org.scalastyle" %% "scalastyle-sbt-plugin" % "0.7.0")

// shows sbt project dependency updates
// https://github.com/rtimush/sbt-updates
addSbtPlugin("com.timushev.sbt" % "sbt-updates" % "0.1.10")

// generates scala file about guild
// https://github.com/sbt/sbt-buildinfo
addSbtPlugin("com.eed3si9n" % "sbt-buildinfo" % "0.5.0")

// allows usage of git in sbt
// https://github.com/sbt/sbt-git
addSbtPlugin("com.typesafe.sbt" % "sbt-git" % "0.8.5")

// sbt automatic releases
// https://github.com/sbt/sbt-release
addSbtPlugin("com.github.gseitz" % "sbt-release" % "1.0.0")

// offers statement and branch coverage
// https://github.com/scoverage/scalac-scoverage-plugin
addSbtPlugin("org.scoverage" % "sbt-scoverage" % "1.5.0")