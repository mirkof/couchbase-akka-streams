

publishTo := {
  val nexus = "http://maven.platform.tm.tmcs:8081/nexus/"
  if (isSnapshot.value)
    Some("snapshots" at nexus + "content/repositories/snapshots")
  else
    Some("releases" at nexus + "content/repositories/releases")
}
publishLocal :=()

//disable cross version (when publish do it without _<scala-version>)
crossVersion := CrossVersion.Disabled


//publish project in maven style
publishMavenStyle := true

//don't publish test artifacts
publishArtifact in Test := false
