[![build status](https://gitlab.com/mirkof/couchbase-akka-streams/badges/master/build.svg)](https://gitlab.com/mirkof/couchbase-akka-streams/commits/master)

# Couchbase Akka Streams
======================================

[Couchbase](https://developer.couchbase.com/documentation/server/current/sdk/java/start-using-sdk.html) Java library wrapper that replaces [rxJava](https://github.com/ReactiveX/RxJava) with [Akka Streams](http://doc.akka.io/docs/akka/2.4/scala/stream/stream-introduction.html).


## How to use it

I would recommend creating a singleton service of Couchbase. Take a look at the code and this [test](https://gitlab.com/mirkof/couchbase-akka-streams/blob/master/src/test/scala/mirkof/couchbase/CouchbaseSpec.scala) as an inspiration.


