import sbt.Keys._

name := "couchbase-akka-streams"

scalaVersion := "2.12.3"

organization := "com.ticketmaster.hailstorm"

lazy val root = (
  Project("couchbase-akka-streams", file("."))
    enablePlugins(BuildInfoPlugin, GitVersioning, GitBranchPrompt)
  )

val akkaVersion = "2.5.3"
val couchbaseVersion = "2.3.7"
val spec2Version = "3.9.4"

libraryDependencies ++= Seq(
  "com.typesafe.akka" %% "akka-actor" % akkaVersion, // akka actors
  "com.typesafe.akka" %% "akka-slf4j" % akkaVersion, // akka logging
  "com.typesafe.akka" %% "akka-stream" % akkaVersion withJavadoc(), // akka streams
  "com.couchbase.client" % "java-client" % couchbaseVersion withSources() withJavadoc(), // java based couchbase driver
  "io.reactivex" %% "rxscala" % "0.26.5" withJavadoc(), // required for couchbase conversions
  "io.reactivex" % "rxjava-reactive-streams" % "1.2.1" withJavadoc(), // required for streams conversions
  "com.typesafe.play" %% "play-json" % "2.6.2",
  "com.typesafe" % "config" % "1.3.1",
  "org.slf4j" % "slf4j-simple" % "1.7.25"
)


// compiler options
incOptions := incOptions.value.withNameHashing(true)
scalacOptions ++= Seq(
  "-encoding", "UTF-8", // force encoding to UTF-8
  "-unchecked", // Enable additional warnings where generated code depends on assumptions.
  "-deprecation", // Emit warning and location for usages of deprecated APIs.
  "-feature", // Emit warning and location for usages of features that should be imported explicitly.
  "-Xfuture", // Turn on future language features.
  "-language:existentials", // Existential types (besides wildcard types) can be written and inferred
  "-language:higherKinds", // Allow higher-kinded types
  "-language:implicitConversions", // Allow definition of implicit functions called views
  "-language:postfixOps", // Allow postfix operator
  //  "-Xfatal-warnings",               // Fail the compilation if there are any warnings
  "-Xlint:-missing-interpolator", // Enable recommended additional warnings.
  "-Yno-adapted-args", // Do not adapt an argument list (either by inserting () or creating a tuple) to match the receiver.
  "-Ywarn-dead-code" // Warn when dead code is identified.
)


// testing
libraryDependencies ++= Seq(
"org.specs2" %% "specs2-core"           % spec2Version % Test withSources() withJavadoc(),
"org.specs2" %% "specs2-common"         % spec2Version % Test withSources() withJavadoc(),
"org.specs2" %% "specs2-junit"          % spec2Version % Test withSources() withJavadoc(),
"org.specs2" %% "specs2-matcher"        % spec2Version % Test withSources() withJavadoc(),
"org.specs2" %% "specs2-matcher-extra"  % spec2Version % Test withSources() withJavadoc(),
"org.specs2" %% "specs2-mock"           % spec2Version % Test withSources() withJavadoc()
)

scalacOptions in Test ++= Seq("-Yrangepos")

// formatting
import scalariform.formatter.preferences._
import com.typesafe.sbt.SbtScalariform.ScalariformKeys

scalariformSettings
ScalariformKeys.preferences := ScalariformKeys.preferences.value
  .setPreference(AlignParameters, true)
  .setPreference(AlignSingleLineCaseStatements, true)
  .setPreference(DoubleIndentClassDeclaration, true)