package mirkof.couchbase.documents

import com.couchbase.client.java.document.{ RawJsonDocument, JsonDocument }
import mirkof.couchbase.utils.JsonConversions
import JsonConversions._
import mirkof.couchbase.types._
import play.api.libs.json._

case class JsDocument(
  id:      CouchbaseID,
  content: Option[JsValue],
  cas:     Option[CAS]     = None,
  expiry:  Option[Expiry]  = None
) extends AbstractDocument[JsValue](id, content, cas, expiry)

object JsDocument {

  implicit val jsDocumentFormat: Format[JsDocument] = Json.format[JsDocument]

  implicit def jsDocument2JsonDocument(js: JsDocument): JsonDocument = JsonDocument.create(js.id, js.expiry, js.content, js.cas)

  implicit def jsonDocument2JsDocument(json: JsonDocument): JsDocument = JsDocument(json.id, json.content, json.cas, json.expiry)

  implicit def jsDocument2RawJsonDocument(js: JsDocument): RawJsonDocument = RawJsonDocument.create(js.id, js.expiry, js.content, js.cas)

  implicit def rawJsonDocument2JsDocument(json: RawJsonDocument): JsDocument = JsDocument(json.id, json.content, json.cas, json.expiry)

}
