package mirkof.couchbase.documents

import com.couchbase.client.java.document.{ JsonLongDocument, RawJsonDocument }
import mirkof.couchbase.types._
import mirkof.couchbase.documents.JsDocument._
import play.api.libs.json._

trait Document[A] {

  def id: CouchbaseID

  def content: Option[A]

  def cas: Option[CAS]

  def expiry: Option[Expiry]
}

abstract class AbstractDocument[A](
    id:      CouchbaseID,
    content: Option[A],
    cas:     Option[CAS]    = None,
    expiry:  Option[Expiry] = None
) extends Document[A] {
  require(id.value.nonEmpty, "The Document ID must not be null or empty.")
  require(id.value.getBytes.length <= 250, "The Document ID must not be larger than 250 bytes")
  require(expiry >= 0, "The Document expiry must not be negative.")
}

sealed case class DefaultDocument[A](
  id:      CouchbaseID,
  content: Option[A],
  cas:     Option[CAS]    = None,
  expiry:  Option[Expiry] = None
) extends AbstractDocument[A](id, content, cas, expiry)

object Document {
  def apply[A](
    id:      CouchbaseID,
    content: Option[A],
    cas:     Option[CAS]    = None,
    expiry:  Option[Expiry] = None
  ): Document[A] = DefaultDocument[A](id, content, cas, expiry)

  def apply[A](id: CouchbaseID, content: A): Document[A] = new DefaultDocument[A](id, Option(content))

  implicit def document2JsDocument[A](doc: Document[A])(implicit wra: Writes[A]): JsDocument =
    JsDocument(doc.id, doc.content.flatMap(x => Json.toJson[A](x).asOpt[JsValue]), doc.cas, doc.expiry)

  implicit def jsDocument2Document[A](jsDoc: JsDocument)(implicit rda: Reads[A]): Document[A] =
    new DefaultDocument(jsDoc.id, jsDoc.content.flatMap(js => Json.fromJson[A](js).asOpt), jsDoc.cas, jsDoc.expiry)

  implicit def document2RawJsonDocument[A](doc: Document[A])(implicit wra: Writes[A]): RawJsonDocument =
    jsDocument2RawJsonDocument(document2JsDocument(doc))

  implicit def rawJsonDocument2Document[A](rawDoc: RawJsonDocument)(implicit rda: Reads[A]): Document[A] =
    jsDocument2Document(rawJsonDocument2JsDocument(rawDoc))

  implicit def fromJsonLongDocument(json: JsonLongDocument): Document[Long] = Document(json.id, Option(json.content.toLong), json.cas, json.expiry)

  implicit def toJsonLongDocument(js: Document[Long]): JsonLongDocument = {
    if (js.content.isDefined) {
      JsonLongDocument.create(js.id, js.expiry, js.content.get, js.cas)
    } else {
      JsonLongDocument.create(js.id)
    }
  }

}
