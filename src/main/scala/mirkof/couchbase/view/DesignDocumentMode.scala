package mirkof.couchbase.view

sealed class DesignDocumentMode(val value: Boolean)

case object ProductionMode extends DesignDocumentMode(false)

case object DevelopmentMode extends DesignDocumentMode(true)
