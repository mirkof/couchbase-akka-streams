package mirkof.couchbase.view

import com.couchbase.client.java.view.{ DesignDocument => JavaDesignDocument, View => JavaView }
import mirkof.couchbase.types.DesignName
import mirkof.couchbase.view.View._
import scala.collection.JavaConverters._

case class DesignDocument(name: DesignName, views: Seq[View]) {
  require(name.value.nonEmpty, "The Design name must not be null or empty.")
}

object DesignDocument {

  implicit def toScalaDesignDocument(jdd: JavaDesignDocument): DesignDocument = DesignDocument(jdd.name, jdd.views.asScala.map(toScalaView))

  implicit def toJavaDesignDocument(sdd: DesignDocument): JavaDesignDocument = JavaDesignDocument.create(sdd.name, sdd.views.map(toJavaView).asJava)

}
