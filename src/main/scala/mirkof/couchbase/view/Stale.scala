package mirkof.couchbase.view

object Stale extends Enumeration {
  type Stale = Value
  val FALSE, TRUE, UPDATE_AFTER = Value
}
