package mirkof.couchbase.view

import akka.stream.scaladsl.Source
import com.couchbase.client.java.document.RawJsonDocument
import com.couchbase.client.java.view.{ AsyncViewResult => JavaAsyncViewResult, AsyncViewRow => JavaAsyncViewRow }
import mirkof.couchbase.documents.Document
import mirkof.couchbase.utils.JsonConversions._
import mirkof.couchbase.utils.RxStreamConversions._
import play.api.libs.json.{ Format, JsValue }
import akka.NotUsed

case class StreamViewResult[A](
  rows:      Source[StreamViewRow[A], NotUsed],
  totalRows: Int,
  success:   Boolean,
  errors:    Source[JsValue, NotUsed],
  debug:     Option[JsValue]
)

object StreamViewResult {
  import StreamViewRow._

  implicit def toScalaAsyncViewResult[A](result: JavaAsyncViewResult)(implicit fmt: Format[A]): StreamViewResult[A] = {
    StreamViewResult(
      rows = observable2SourceWith(result.rows()),
      totalRows = result.totalRows(),
      success = result.success(),
      errors = observable2SourceWith(result.error()).mapConcat[JsValue](_.toList),
      debug = result.debug()
    )
  }

}

case class StreamViewRow[A](
  id:       String,
  key:      Any,
  value:    Any,
  document: Source[Document[A], NotUsed]
)

object StreamViewRow {

  implicit def toScalaAsyncViewRow[A](row: JavaAsyncViewRow)(implicit fmt: Format[A]): StreamViewRow[A] =
    StreamViewRow(
      row.id(),
      row.key(),
      row.value(),
      row.document(classOf[RawJsonDocument])
    )
}
