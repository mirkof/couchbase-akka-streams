package mirkof.couchbase.view

import akka.NotUsed
import akka.stream.scaladsl.Source
import com.couchbase.client.java.query.{ N1qlMetrics, AsyncN1qlQueryResult => JavaAsyncN1qlQueryResult, AsyncN1qlQueryRow => JavaAsyncN1qlQueryRow }
import mirkof.couchbase.utils.JsonConversions._
import mirkof.couchbase.utils.RxStreamConversions._
import play.api.libs.json.{ Format, JsObject, JsValue, Json }

case class StreamN1qlQueryResult[A](
  rows:            Source[StreamN1qlQueryRow[A], NotUsed],
  signature:       Source[AnyRef, NotUsed],
  info:            Source[N1qlMetrics, NotUsed],
  parseSuccess:    Boolean,
  status:          Source[String, NotUsed],
  finalSuccess:    Source[Boolean, NotUsed],
  errors:          Source[JsValue, NotUsed],
  requestId:       String,
  clientContextId: String
)

object StreamN1qlQueryResult {

  import StreamN1qlQueryRow._

  implicit def toScalaN1qlQueryResult[A](result: JavaAsyncN1qlQueryResult)(implicit fmt: Format[A]): StreamN1qlQueryResult[A] =
    StreamN1qlQueryResult(
      rows = observable2SourceWith(result.rows()),
      signature = observable2SourceWith(result.signature()),
      info = result.info(),
      parseSuccess = result.parseSuccess(),
      status = result.status(),
      finalSuccess = observable2SourceWith(result.finalSuccess()),
      errors = observable2SourceWith(result.errors()).mapConcat[JsValue](_.toList),
      requestId = result.requestId(),
      clientContextId = result.clientContextId()
    )

}

case class StreamN1qlQueryRow[A](
  byteValue: Array[Byte],
  value:     Option[A]
)

object StreamN1qlQueryRow {

  implicit def toScalaN1qlQueryRow[A](row: JavaAsyncN1qlQueryRow)(implicit fmt: Format[A]): StreamN1qlQueryRow[A] =
    StreamN1qlQueryRow(
      byteValue = row.byteValue(),
      value = jsonObject2JsValueOpt(row.value()).flatMap { js =>
        // for some reason the structure is { bucket_name : value }
        js.as[JsObject].values.flatMap { jsValue =>
          Json.fromJson[A](jsValue).asOpt
        }.headOption
      }
    )
}
