package mirkof.couchbase.view

import com.couchbase.client.java.document.json.{ JsonArray, JsonObject }
import com.couchbase.client.java.view.{ Stale => JavaStale, ViewQuery => JavaViewQuery }
import mirkof.couchbase.types.{ DesignName, ViewName }
import mirkof.couchbase.view.Stale.Stale
import play.api.libs.json.{ JsArray, JsObject }

object ViewQuery {
  def apply(designDocument: DesignName, view: ViewName): ViewQuery = new ViewQuery(designDocument, view)
}

// scalastyle:off number.of.methods
case class ViewQuery protected (designDocument: DesignName, view: ViewName, jvq: JavaViewQuery) {

  def this(designDocument: DesignName, view: ViewName) = {
    this(designDocument, view, JavaViewQuery.from(designDocument, view))
  }

  protected def this(jvq: JavaViewQuery) = {
    this(jvq.getDesign, jvq.getView, jvq)
  }

  protected implicit def fromJavaViewQuery(jvq: JavaViewQuery): ViewQuery = new ViewQuery(jvq)

  def debug(): ViewQuery = jvq.debug()

  def debug(debug: Boolean): ViewQuery = jvq.debug(debug)

  def descending(): ViewQuery = jvq.descending()

  def descending(descending: Boolean): ViewQuery = jvq.descending(descending)

  def development(): ViewQuery = jvq.development()

  def development(development: Boolean): ViewQuery = jvq.development(development)

  def isDevelopment: Boolean = jvq.isDevelopment

  def key(key: String): ViewQuery = jvq.key(key)

  def key(key: Int): ViewQuery = jvq.key(key)

  def key(key: Long): ViewQuery = jvq.key(key)

  def key(key: Double): ViewQuery = jvq.key(key)

  def key(key: Boolean): ViewQuery = jvq.key(key)

  def keys(keys: JsArray): ViewQuery = jvq.keys(JsonArray.fromJson(keys.toString))

  def startKey(key: String): ViewQuery = jvq.startKey(key)

  def startKey(key: Int): ViewQuery = jvq.startKey(key)

  def startKey(key: Long): ViewQuery = jvq.startKey(key)

  def startKey(key: Double): ViewQuery = jvq.startKey(key)

  def startKey(key: Boolean): ViewQuery = jvq.startKey(key)

  def startKey(key: JsArray): ViewQuery = jvq.startKey(JsonArray.fromJson(key.toString))

  def startKey(key: JsObject): ViewQuery = jvq.startKey(JsonObject.fromJson(key.toString))

  def endKey(key: String): ViewQuery = jvq.endKey(key)

  def endKey(key: Int): ViewQuery = jvq.endKey(key)

  def endKey(key: Long): ViewQuery = jvq.endKey(key)

  def endKey(key: Double): ViewQuery = jvq.endKey(key)

  def endKey(key: Boolean): ViewQuery = jvq.endKey(key)

  def endKey(key: JsArray): ViewQuery = jvq.endKey(JsonArray.fromJson(key.toString))

  def endKey(key: JsObject): ViewQuery = jvq.endKey(JsonObject.fromJson(key.toString))

  def startKeyDocId(id: String): ViewQuery = jvq.startKeyDocId(id)

  def endKeyDocId(id: String): ViewQuery = jvq.endKeyDocId(id)

  def inclusiveEnd(): ViewQuery = jvq.inclusiveEnd()

  def inclusiveEnd(inclusiveEnd: Boolean): ViewQuery = jvq.inclusiveEnd(inclusiveEnd)

  def limit(limit: Int): ViewQuery = jvq.limit(limit)

  def skip(skip: Int): ViewQuery = jvq.skip(skip)

  def reduce(): ViewQuery = jvq.reduce()

  def reduce(reduce: Boolean): ViewQuery = jvq.reduce(reduce)

  def group(): ViewQuery = jvq.group()

  def group(group: Boolean): ViewQuery = jvq.debug(group)

  def groupLevel(groupLevel: Int): ViewQuery = jvq.groupLevel(groupLevel)

  def stale(stale: Stale): ViewQuery = {
    val enumStale = JavaStale.valueOf(stale.toString)

    jvq.stale(enumStale)
  }

  override def toString: String = jvq.toString
}
// scalastyle:on number.of.methods
