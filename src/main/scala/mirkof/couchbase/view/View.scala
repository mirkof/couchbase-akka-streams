package mirkof.couchbase.view

import com.couchbase.client.java.view.{ DefaultView, View => JavaView }
import mirkof.couchbase.types.ViewName

case class View(name: ViewName, map: String, reduce: Option[String] = None) {
  require(name.value.nonEmpty, "The View name must not be null or empty.")
  require(map.nonEmpty, "The map function must not be null or empty.")
  require(compileJS(map), "The map function must be valid javascript.")

  private def compileJS(script: String): Boolean = {
    // java8 nashorn

    import javax.script.ScriptEngineManager
    import scala.util.Try

    // scalastyle:off null
    val engineManager = new ScriptEngineManager(null)
    val engine = engineManager.getEngineByName("nashorn")
    assert(engine != null, "nashorn engine is null")
    // scalastyle:on null

    Try(engine.eval(script)).isSuccess
  }
}

object View {

  implicit def toScalaView(view: JavaView): View =
    View(
      view.name,
      view.map,
      Option(view.reduce).flatMap(x => if (x == "null") None else Option(x))
    )

  implicit def toJavaView(view: View): JavaView = DefaultView.create(view.name, view.map, view.reduce.orNull)

}
