package mirkof.couchbase

import akka.NotUsed
import akka.stream.scaladsl.Source
import com.couchbase.client.java.cluster.{ BucketSettings, ClusterInfo, AsyncClusterManager => JavaAsyncClusterManager }
import mirkof.couchbase.utils.RxStreamConversions._

class StreamClusterManager(manager: JavaAsyncClusterManager) {

  def info: Source[ClusterInfo, NotUsed] = observable2Source(manager.info)

  def getBuckets: Source[BucketSettings, NotUsed] = observable2SourceWith(manager.getBuckets)

  def getBucket(name: String): Source[BucketSettings, NotUsed] = observable2SourceWith(manager.getBucket(name))

  def hasBucket(name: String): Source[Boolean, NotUsed] = {
    import rx.lang.scala.JavaConversions._

    observable2SourceWith {
      toScalaObservable(manager.getBuckets).exists(_.name == name)
        .zip(manager.hasBucket(name))
        .map { case (exists, has) => exists && has }
    }
  }

  def insertBucket(settings: BucketSettings): Source[BucketSettings, NotUsed] = observable2SourceWith(manager.insertBucket(settings))

  def updateBucket(settings: BucketSettings): Source[BucketSettings, NotUsed] = observable2SourceWith(manager.updateBucket(settings))

  def removeBucket(name: String): Source[Boolean, NotUsed] = observable2SourceWith(manager.removeBucket(name))

}

object StreamClusterManager {
  implicit def toScalaStreamClusterManager(manager: JavaAsyncClusterManager): StreamClusterManager = new StreamClusterManager(manager)
}
