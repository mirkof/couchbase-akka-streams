package mirkof.couchbase

import akka.NotUsed
import akka.stream.scaladsl.Source
import com.couchbase.client.java.bucket.{ AsyncBucketManager => JavaAsyncBucketManager, BucketInfo }
import mirkof.couchbase.utils.RxStreamConversions._
import mirkof.couchbase.view.{ DesignDocument, DesignDocumentMode, ProductionMode }
import mirkof.couchbase.view.DesignDocument._

class StreamBucketManager(bucketManager: JavaAsyncBucketManager) {

  def info: Source[BucketInfo, NotUsed] = observable2Source(bucketManager.info)

  def flush: Source[Boolean, NotUsed] = observable2SourceWith(bucketManager.flush)

  def getDesignDocuments(mode: DesignDocumentMode = ProductionMode): Source[DesignDocument, NotUsed] =
    observable2SourceWith(bucketManager.getDesignDocuments(mode.value))

  def getDesignDocument(name: String, mode: DesignDocumentMode = ProductionMode): Source[DesignDocument, NotUsed] =
    observable2SourceWith(bucketManager.getDesignDocument(name, mode.value))

  def insertDesignDocument(designDocument: DesignDocument, mode: DesignDocumentMode = ProductionMode): Source[DesignDocument, NotUsed] =
    observable2SourceWith(bucketManager.insertDesignDocument(designDocument, mode.value))

  def upsertDesignDocument(designDocument: DesignDocument, mode: DesignDocumentMode = ProductionMode): Source[DesignDocument, NotUsed] =
    observable2SourceWith(bucketManager.upsertDesignDocument(designDocument, mode.value))

  def removeDesignDocument(name: String, mode: DesignDocumentMode = ProductionMode): Source[Boolean, NotUsed] =
    observable2SourceWith(bucketManager.removeDesignDocument(name, mode.value))

  def publishDesignDocument(name: String, overwrite: Boolean = false): Source[DesignDocument, NotUsed] =
    observable2SourceWith(bucketManager.publishDesignDocument(name, overwrite))

  def createN1qlPrimaryIndex(ignoreIfExist: Boolean, defer: Boolean): Source[Boolean, NotUsed] =
    observable2SourceWith(bucketManager.createN1qlPrimaryIndex(ignoreIfExist, defer))

}

object StreamBucketManager {
  implicit def toScalaStreamManager(manager: JavaAsyncBucketManager): StreamBucketManager = new StreamBucketManager(manager)
}

