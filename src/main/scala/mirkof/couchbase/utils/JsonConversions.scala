package mirkof.couchbase.utils

import com.couchbase.client.java.document.json.JsonObject
import play.api.libs.json.{ JsValue, Json }

object JsonConversions {
  implicit def jsValueOpt2JsonObject(maybeJs: Option[JsValue]): JsonObject = maybeJs.map { js =>
    JsonObject.fromJson(Json.stringify(js))
  }.orNull

  implicit def jsonObject2JsValueOpt(json: JsonObject): Option[JsValue] = Option(json).map { json =>
    Json.parse(json.toString).asOpt[JsValue]
  }.getOrElse(None)

  implicit def jsValueOpt2String(maybeJs: Option[JsValue]): String = maybeJs.map(Json.stringify).orNull

  implicit def string2JsValueOpt(str: String): Option[JsValue] = Option(str).flatMap(js => Json.parse(js).asOpt[JsValue])
}
