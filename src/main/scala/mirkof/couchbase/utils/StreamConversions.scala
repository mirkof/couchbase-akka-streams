package mirkof.couchbase.utils

import akka.NotUsed
import akka.stream.Materializer
import akka.stream.scaladsl.{ Flow, Sink, Source }

import scala.concurrent.Future

/**
 * Helper methods for quickly converting [[akka.stream.scaladsl.Source]] to [[scala.concurrent.Future]]
 */
object StreamConversions {

  implicit class Source2Future[A](source: Source[A, NotUsed])(implicit materializer: Materializer) {

    def toFuture: Future[A] = source.runWith(Sink.head)

    def toFutureOption: Future[Option[A]] = source.runWith(Sink.headOption)

    def toFutureSeq: Future[Seq[A]] = source.runWith(Sink.seq)
  }

  def optionalFlow[A]: Flow[Option[A], A, NotUsed] = Flow[Option[A]].flatMapConcat { maybe =>
    maybe.map { value =>
      Source.single(value)
    }.getOrElse {
      Source.empty
    }
  }
}

