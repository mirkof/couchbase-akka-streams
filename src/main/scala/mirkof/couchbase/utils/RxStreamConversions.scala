package mirkof.couchbase.utils

import mirkof.couchbase.documents.Document
import mirkof.couchbase.documents.Document._
import akka.NotUsed
import akka.stream.Materializer
import akka.stream.scaladsl._
import com.couchbase.client.java.document.RawJsonDocument
import com.couchbase.client.java.document.json.JsonObject
import com.couchbase.client.java.query.{ AsyncN1qlQueryResult, AsyncN1qlQueryRow }
import com.couchbase.client.java.view.{ AsyncViewResult, AsyncViewRow }
import org.slf4j.LoggerFactory
import play.api.libs.json.Format
import rx.RxReactiveStreams
import rx.lang.scala.ImplicitFunctionConversions.scalaFunction1ToRxFunc1
import rx.lang.scala.schedulers.ExecutionContextScheduler

import scala.concurrent.{ ExecutionContext, Future, Promise }

object RxStreamConversions {
  protected lazy val logger = LoggerFactory.getLogger(getClass)

  def observable2Source[A](obs: rx.Observable[_ <: A]): Source[A, NotUsed] = {
    Source.fromPublisher(RxReactiveStreams.toPublisher(obs.onBackpressureBuffer()))
  }

  implicit def observable2SourceWith[A, B](obs: rx.Observable[_ <: A])(implicit convert: (A) => B): Source[B, NotUsed] = {
    observable2Source(obs).map(convert)
  }

  implicit def observable2SourceDocument[A](result: rx.Observable[RawJsonDocument])(implicit fmt: Format[A]): Source[Document[A], NotUsed] = {
    observable2Source(result).map(rawJsonDocument2Document[A])
  }

  def observable2FutureDocument[A](result: rx.Observable[RawJsonDocument])(implicit fmt: Format[A], materializer: Materializer): Future[Document[A]] = {
    observable2SourceDocument(result).runWith(Sink.head)
  }

  def observable2FutureOptionDocument[A](result: rx.Observable[RawJsonDocument])(implicit fmt: Format[A], materializer: Materializer): Future[Option[Document[A]]] = {
    observable2SourceDocument(result).runWith(Sink.headOption)
  }

  import rx.lang.scala.JavaConverters._

  def observable2Future[T](obs: rx.Observable[_ <: T])(implicit ec: ExecutionContext): Future[T] = {
    val promise = Promise[T]()

    obs.asScala
      .subscribeOn(ExecutionContextScheduler(ec))
      .subscribe(
        onNext => promise.success(onNext),
        e => promise.failure(e)
      )

    promise.future
  }

  def observable2FutureWith[A, B](obs: rx.Observable[_ <: A])(implicit convert: (A) => B, ec: ExecutionContext): Future[B] = {
    observable2Future(obs).map(convert)
  }

  def observable2FutureOptionWith[A, B](obs: rx.Observable[_ <: A])(implicit convert: (A) => B, ec: ExecutionContext): Future[Option[B]] = {
    observable2Future {
      obs.map[Option[B]]((a: A) => Option(convert(a)))
        .switchIfEmpty(rx.Observable.just(None))
    }
  }

  def logErrors(errors: rx.Observable[JsonObject]): Unit =
    errors.asScala.foreach { (error: JsonObject) => logger.warn(s"query error: $error") }

  def viewResult2Content[A](query: rx.Observable[AsyncViewResult])(implicit fmt: Format[A]): rx.Observable[A] = {
    import scala.collection.JavaConverters._

    query
      .onBackpressureBuffer()
      .flatMap[A]((result: AsyncViewResult) => {
        logErrors(result.error())

        result.rows
          .flatMap[A]((row: AsyncViewRow) =>
            row.document(classOf[RawJsonDocument])
              .flatMap[A]((doc: RawJsonDocument) =>
                rx.Observable.from[A](rawJsonDocument2Document[A](doc).content.toList.asJava)))
      })
  }

  def observableViewQuery2Source[A](query: rx.Observable[AsyncViewResult])(implicit fmt: Format[A]): Source[A, NotUsed] = {
    observable2Source(viewResult2Content[A](query))
  }

  def n1qlResult2Content[A](query: rx.Observable[AsyncN1qlQueryResult])(implicit fmt: Format[A]): rx.Observable[A] = {
    import mirkof.couchbase.view.StreamN1qlQueryRow._

    import scala.collection.JavaConverters._

    query
      .onBackpressureBuffer()
      .flatMap[A]((result: AsyncN1qlQueryResult) => {
        logErrors(result.errors())

        result.rows
          .flatMap[A]((row: AsyncN1qlQueryRow) =>
            rx.Observable.from[A](toScalaN1qlQueryRow[A](row).value.toList.asJava))
      })
  }

  def observableN1qlQuery2Source[A](query: rx.Observable[AsyncN1qlQueryResult])(implicit fmt: Format[A]): Source[A, NotUsed] = {
    observable2Source(n1qlResult2Content[A](query))
  }

}
