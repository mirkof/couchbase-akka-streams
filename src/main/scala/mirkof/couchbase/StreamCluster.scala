package mirkof.couchbase

import com.couchbase.client.java.{ AsyncCluster => JavaAsyncCluster }
import mirkof.couchbase.utils.RxStreamConversions.observable2SourceWith
import akka.NotUsed
import akka.stream.scaladsl.Source

class StreamCluster(cluster: JavaAsyncCluster) {

  def openBucket(): Source[StreamBucket, NotUsed] =
    observable2SourceWith(cluster.openBucket)

  def openBucket(name: String): Source[StreamBucket, NotUsed] =
    observable2SourceWith(cluster.openBucket(name))

  def openBucket(name: String, password: String): Source[StreamBucket, NotUsed] =
    observable2SourceWith(cluster.openBucket(name, password))

  def clusterManager(username: String, password: String): Source[StreamClusterManager, NotUsed] =
    observable2SourceWith(cluster.clusterManager(username, password))

  def disconnect(): Source[Boolean, NotUsed] = observable2SourceWith(cluster.disconnect())

}
