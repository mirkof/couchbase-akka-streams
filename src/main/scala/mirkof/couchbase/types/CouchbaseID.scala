package mirkof.couchbase.types

import play.api.libs.json.Json

case class CouchbaseID(value: String) extends AnyVal {
  override def toString: String = value
}

object CouchbaseID {

  implicit def stringToCouchbaseID(value: String): CouchbaseID = new CouchbaseID(value)

  implicit def couchbaseIDToString(id: CouchbaseID): String = id.value

  implicit val couchbaseIDFormat = Json.format[CouchbaseID]
}

