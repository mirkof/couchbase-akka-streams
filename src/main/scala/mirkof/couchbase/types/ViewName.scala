package mirkof.couchbase.types

case class ViewName(value: String) extends AnyVal {
  override def toString: String = value
}

object ViewName {
  implicit def stringToDesignDocumentName(value: String): ViewName = new ViewName(value)

  implicit def designDocumentNameToString(name: ViewName): String = name.value

}
