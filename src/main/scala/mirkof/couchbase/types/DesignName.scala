package mirkof.couchbase.types

case class DesignName(value: String) extends AnyVal {
  override def toString: String = value
}

object DesignName {
  implicit def stringToDesignDocumentName(value: String): DesignName = new DesignName(value)

  implicit def designDocumentNameToString(name: DesignName): String = name.value
}
