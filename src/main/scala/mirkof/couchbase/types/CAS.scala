package mirkof.couchbase.types

import play.api.libs.json.Json

case class CAS(value: Long) extends AnyVal

object CAS {

  implicit def fromCASOpt(cas: Option[CAS]): Long = cas.getOrElse(CAS(0)).value

  implicit def toCASOpt(value: Long): Option[CAS] = if (value > 0) Option(CAS(value)) else None

  implicit val casFormat = Json.format[CAS]
}
