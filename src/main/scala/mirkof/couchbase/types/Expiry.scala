package mirkof.couchbase.types

import play.api.libs.json.Json

import scala.concurrent.duration.Duration

case class Expiry(value: Int) extends AnyVal

object Expiry {
  def apply(value: Duration): Expiry = new Expiry(value.toSeconds.toInt)

  implicit def fromExpiry(expiry: Expiry): Int = expiry.value
  implicit def fromExpiryOpt(expiry: Option[Expiry]): Int = expiry.getOrElse(Expiry(0)).value

  implicit def toExpiryOpt(value: Int): Option[Expiry] = if (value > 0) Option(Expiry(value)) else None

  implicit val expiryFormat = Json.format[Expiry]

}
