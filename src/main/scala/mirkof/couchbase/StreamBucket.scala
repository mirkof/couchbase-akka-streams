package mirkof.couchbase

import akka.NotUsed
import akka.stream.scaladsl.Source
import com.couchbase.client.java.document.{ JsonDocument, RawJsonDocument }
import com.couchbase.client.java.query.N1qlQuery
import com.couchbase.client.java.{ AsyncBucket => JavaAsyncBucket }
import mirkof.couchbase.documents.Document
import mirkof.couchbase.documents.Document._
import mirkof.couchbase.documents.JsDocument._
import mirkof.couchbase.types._
import mirkof.couchbase.utils.RxStreamConversions._
import mirkof.couchbase.utils._
import mirkof.couchbase.view.{ StreamN1qlQueryResult, StreamViewResult, ViewQuery }
import mirkof.couchbase.utils.StreamConversions.optionalFlow
import play.api.libs.json.Format

class StreamBucket(bucket: JavaAsyncBucket) {

  def name: String = bucket.name

  def rx: JavaAsyncBucket = bucket

  def get[A](id: CouchbaseID)(implicit fmt: Format[A]): Source[Document[A], NotUsed] =
    bucket.get(id.value, classOf[RawJsonDocument])

  def getDirect[A](id: CouchbaseID)(implicit fmt: Format[A]): Source[A, NotUsed] =
    get[A](id).map(_.content).flatMapConcat { maybe =>
      maybe.map { value =>
        Source.single(value)
      }.getOrElse {
        Source.empty
      }
    }

  def get[A](d: Document[A])(implicit fmt: Format[A]): Source[Document[A], NotUsed] =
    bucket.get[RawJsonDocument](d)

  def getAndLock[A](id: CouchbaseID, lockTime: Int)(implicit fmt: Format[A]): Source[Document[A], NotUsed] =
    bucket.getAndLock(id.value, lockTime, classOf[RawJsonDocument])

  def getAndLock[A](d: Document[A], lockTime: Int)(implicit fmt: Format[A]): Source[Document[A], NotUsed] =
    bucket.getAndLock[RawJsonDocument](d, lockTime)

  def getAndTouch[A](id: CouchbaseID, expiry: Expiry)(implicit fmt: Format[A]): Source[Document[A], NotUsed] =
    bucket.getAndTouch(id.value, expiry, classOf[RawJsonDocument])

  def getAndTouch[A](d: Document[A])(implicit fmt: Format[A]): Source[Document[A], NotUsed] =
    bucket.getAndTouch[RawJsonDocument](d)

  def insert[A](d: Document[A])(implicit fmt: Format[A]): Source[Document[A], NotUsed] =
    bucket.insert[RawJsonDocument](d)

  def upsert[A](d: Document[A])(implicit fmt: Format[A]): Source[Document[A], NotUsed] =
    bucket.upsert[RawJsonDocument](d)

  def replace[A](d: Document[A])(implicit fmt: Format[A]): Source[Document[A], NotUsed] =
    bucket.replace[RawJsonDocument](d)

  def remove[A](d: Document[A])(implicit fmt: Format[A]): Source[Document[A], NotUsed] =
    bucket.remove[RawJsonDocument](d)

  def remove[A](id: CouchbaseID)(implicit fmt: Format[A]): Source[Document[A], NotUsed] =
    RxStreamConversions.observable2Source[JsonDocument](bucket.remove(id.value))
      .map(jsonDocument2JsDocument)
      .map(jsDocument2Document[A])

  def unlock(id: CouchbaseID, cas: CAS): Source[Boolean, NotUsed] =
    observable2SourceWith(bucket.unlock(id.value, cas.value))

  def unlock[A](d: Document[A])(implicit fmt: Format[A]): Source[Boolean, NotUsed] =
    observable2SourceWith(bucket.unlock[RawJsonDocument](d))

  def touch(id: CouchbaseID, expiry: Expiry): Source[Boolean, NotUsed] =
    observable2SourceWith(bucket.touch(id.value, expiry))

  def touch[A](d: Document[A])(implicit fmt: Format[A]): Source[Boolean, NotUsed] =
    observable2SourceWith(bucket.touch[RawJsonDocument](d))

  def query[A](viewQuery: ViewQuery)(implicit fmt: Format[A]): Source[StreamViewResult[A], NotUsed] =
    observable2SourceWith(bucket.query(viewQuery.jvq))

  def queryDirect[A](viewQuery: ViewQuery)(implicit fmt: Format[A]): Source[A, NotUsed] =
    observableViewQuery2Source[A](bucket.query(viewQuery.jvq))

  def query[A](n1qlQuery: N1qlQuery)(implicit fmt: Format[A]): Source[StreamN1qlQueryResult[A], NotUsed] =
    observable2SourceWith(bucket.query(n1qlQuery))

  def queryDirect[A](n1qlQuery: N1qlQuery)(implicit fmt: Format[A]): Source[A, NotUsed] =
    observableN1qlQuery2Source[A](bucket.query(n1qlQuery))

  def counter(id: CouchbaseID, delta: Long): Source[Document[Long], NotUsed] =
    observable2SourceWith(bucket.counter(id, delta))

  def counter(id: CouchbaseID, delta: Long, initial: Long): Source[Document[Long], NotUsed] =
    observable2SourceWith(bucket.counter(id, delta, initial))

  def counter(id: CouchbaseID, delta: Long, initial: Long, expiry: Expiry): Source[Document[Long], NotUsed] =
    observable2SourceWith(bucket.counter(id, delta, initial, expiry))

  def close(): Source[Boolean, NotUsed] = observable2SourceWith(bucket.close())

  def manager: Source[StreamBucketManager, NotUsed] = observable2SourceWith(bucket.bucketManager)

}

object StreamBucket {
  implicit def toScalaStreamBucket(bucket: JavaAsyncBucket): StreamBucket = new StreamBucket(bucket)
}
