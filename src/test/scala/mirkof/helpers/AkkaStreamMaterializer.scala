package mirkof.helpers

import akka.actor.ActorSystem
import akka.stream.{ ActorMaterializer, Materializer }

object AkkaStreamMaterializer {
  implicit lazy val actorSystem: ActorSystem = ActorSystem("stream")
  implicit lazy val materializer: Materializer = ActorMaterializer(namePrefix = Option("Flow"))
}
