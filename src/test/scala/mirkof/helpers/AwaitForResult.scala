package mirkof.helpers

import java.util.concurrent.TimeoutException

import org.specs2.concurrent.ExecutionEnv

import scala.concurrent.duration._
import scala.concurrent.{ Await, Future }

trait AwaitForResult {

  val longAwaitTimeout = 10 seconds

  /**
   * pimped out Await.result to look like specs2 FutureMatchable
   */
  implicit class FutureResult[T](f: Future[T])(implicit ee: ExecutionEnv) {
    def await: T = await(retries = 0, timeout = 1.second)

    def retry(retries: Int): T = await(retries, timeout = 1.second)

    def awaitFor(timeout: FiniteDuration): T = await(retries = 0, timeout)

    def await(retries: Int, timeout: FiniteDuration): T = {
      val tf = ee.timeFactor
      val appliedTimeout = timeout * tf.toLong

      def awaitFuture(remainingRetries: Int, totalDuration: FiniteDuration): T = {
        try Await.result(f, appliedTimeout)
        catch {
          case e if e.getClass == classOf[TimeoutException] =>
            if (remainingRetries <= 0) {
              throw new TimeoutException(s"Timeout after ${totalDuration + appliedTimeout} (retries = $retries, timeout = $timeout)")
            } else {
              awaitFuture(remainingRetries - 1, totalDuration + appliedTimeout)
            }
          case other: Throwable => throw other
        }
      }
      awaitFuture(retries, 0.second)
    }
  }

}
