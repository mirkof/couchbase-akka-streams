package mirkof.couchbase

import com.couchbase.client.java.CouchbaseAsyncCluster
import mirkof.helpers.AkkaStreamMaterializer
import org.specs2.concurrent.ExecutionEnv
import org.specs2.specification.BeforeAll
import com.typesafe.config._
import org.specs2.Specification
import org.specs2.matcher.ThrownExpectations
import mirkof.couchbase.utils.StreamConversions._
import mirkof.helpers.{ AkkaStreamMaterializer, AwaitForResult }

abstract class CouchbaseSpec(implicit ee: ExecutionEnv)
    extends Specification
    with ThrownExpectations
    with BeforeAll
    with AwaitForResult {

  val config = ConfigFactory.load("test.conf")

  final lazy val user = config.getString("couchbase.admin.user")
  final lazy val pass = config.getString("couchbase.admin.pass")
  final lazy val hosts = config.getStringList("couchbase.hosts")

  var cluster: StreamCluster = _
  var clusterManager: StreamClusterManager = _
  var bucket: StreamBucket = _
  var bucketManager: StreamBucketManager = _

  implicit lazy val actorSystem = AkkaStreamMaterializer.actorSystem
  implicit lazy val materializer = AkkaStreamMaterializer.materializer

  override def beforeAll: Unit = {
    cluster = new StreamCluster(CouchbaseAsyncCluster.create(hosts))
    clusterManager = cluster.clusterManager(user, pass).toFuture.awaitFor(longAwaitTimeout)
    bucket = cluster.openBucket().toFuture.awaitFor(longAwaitTimeout)
    bucketManager = bucket.manager.toFuture.awaitFor(longAwaitTimeout)
  }

}
