package mirkof.couchbase

import com.couchbase.client.java.error.DesignDocumentAlreadyExistsException
import mirkof.couchbase.types.DesignName
import mirkof.couchbase.view.{ DesignDocument, DesignDocumentMode, DevelopmentMode, ProductionMode, _ }
import org.specs2.concurrent._
import mirkof.couchbase.utils.StreamConversions._
import scala.concurrent.Future

class StreamBucketManagerSpec(implicit ee: ExecutionEnv) extends CouchbaseSpec {
  // scalastyle:off public.methods.have.type
  def is = sequential ^ s2"""
    A StreamBucketManager should
      insert a design                           $e1
      check if created design exists            $e2
      fail to insert design twice               $e3
      get a design                              $e4
      publish a design to production            $e5
      remove a design from production           $e6
      update a design                           $e7
      remove a design                           $e8

    A StreamBucketManager should throw error if
      view name is empty                        $e9
      map function is empty                     $e10
      map function is invalid                   $e11
      design document name is empty             $e12
      """

  val docName = "testDesign"
  val viewName = "testView"
  val mode = DevelopmentMode

  val view = View(
    name = viewName,
    map = s"""function (doc, meta) {
             |emit(doc, null)
             |}
        """.stripMargin
  )

  val designDocument = DesignDocument(
    name = docName,
    views = Seq(view)
  )

  private def hasDocument(name: DesignName, mode: DesignDocumentMode): Future[Boolean] = {
    bucketManager.getDesignDocuments(mode).filter(_.name == name).toFutureOption.map(_.isDefined)
  }

  def e1 = {
    bucketManager.insertDesignDocument(designDocument, mode).toFuture must beEqualTo(designDocument).await
  }

  def e2 = {
    hasDocument(docName, mode) must beTrue.await
  }

  def e3 = {
    bucketManager.insertDesignDocument(designDocument, mode).toFuture must throwA[DesignDocumentAlreadyExistsException].await
  }

  def e4 = {
    bucketManager.getDesignDocument(docName, mode).toFuture must beEqualTo(designDocument).await
  }

  def e5 = {
    hasDocument(docName, DevelopmentMode) must beTrue.await

    hasDocument(docName, ProductionMode) must beFalse.await

    bucketManager.publishDesignDocument(docName).toFuture must beEqualTo(designDocument).await

    hasDocument(docName, DevelopmentMode) must beTrue.await

    hasDocument(docName, ProductionMode) must beTrue.await
  }

  def e6 = {
    bucketManager.removeDesignDocument(docName, ProductionMode).toFuture must beTrue.await

    hasDocument(docName, ProductionMode) must beFalse.await
  }

  def e7 = {
    val newView = view.copy(name = viewName + "count", reduce = Option("_count"))

    val newDesign = designDocument.copy(views = Seq(newView))

    bucketManager.upsertDesignDocument(newDesign, mode).toFuture must beEqualTo(newDesign).awaitFor(longAwaitTimeout)

    hasDocument(docName, mode) must beTrue.await

    bucketManager.getDesignDocument(docName, mode).toFuture must beEqualTo(newDesign).await
  }

  def e8 = {
    bucketManager.removeDesignDocument(docName, mode).toFuture must beTrue.await

    hasDocument(docName, mode) must beFalse.await
  }

  def e9 = {
    View("", view.map) must throwA[IllegalArgumentException]
  }

  def e10 = {
    View(view.name, "") must throwA[IllegalArgumentException]
  }

  def e11 = {
    View(view.name, "function (doc, meta) { invalid json") must throwA[IllegalArgumentException]
  }

  def e12 = {
    DesignDocument("", Seq(view)) must throwA[IllegalArgumentException]
  }

}
