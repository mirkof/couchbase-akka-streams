package mirkof.couchbase

import com.couchbase.client.java.error.{ CASMismatchException, DocumentAlreadyExistsException }
import mirkof.couchbase.documents.{ Document, JsDocument }
import mirkof.couchbase.types.CouchbaseID
import org.specs2.concurrent._
import mirkof.couchbase.utils.StreamConversions._

import scala.util.Random

class StreamBucketSpec(implicit ee: ExecutionEnv) extends CouchbaseSpec {
  // scalastyle:off public.methods.have.type
  def is = sequential ^ s2"""
    A StreamBucket should
      allow a document to be inserted                           $e1
      fail to insert document twice                             $e2
      be able to get a document                                 $e3
      fail to get a non existing document                       $e4
      allow a document to be updated                            $e5
      allow a document to be removed                            $e6
      support CAS failure on replace                            $e7
      """

  def randomDoc(id: String, content: Int = 0): Document[Int] = {
    val rand = Random.nextInt(Int.MaxValue)
    val cid: CouchbaseID = id + rand
    val num: Int = if (content == 0) rand else content
    Document(cid, num)
  }

  def e1 = {
    val doc = randomDoc("testInsert")

    bucket.insert(doc).map(_.content).toFuture must beEqualTo(doc.content).await
  }

  def e2 = {
    val doc = randomDoc("testInsert2")

    bucket.insert(doc).map(_.content).toFuture must beEqualTo(doc.content).await

    bucket.insert(doc).toFuture must throwA[DocumentAlreadyExistsException].await
  }

  def e3 = {
    val doc = randomDoc("testGet")

    bucket.insert(doc).map(_.content).toFuture must beEqualTo(doc.content).await

    bucket.getDirect[Int](doc.id).toFutureOption must beEqualTo(doc.content).await
  }

  def e4 = {
    val id: CouchbaseID = "testFailGet"

    bucket.get[Int](id).toFutureOption must beNone.await
  }

  def e5 = {
    val doc = randomDoc("testUpdate", 1)

    // Insert a document containing 1 and check it's created
    bucket.insert(doc).map(_.content).toFuture must beEqualTo(doc.content).await

    val newDoc = Document(doc.id, Option(2), doc.cas)
    // Replace it with one containing 2
    bucket.replace(newDoc).map(_.content).toFuture must beEqualTo(newDoc.content).await

    // check that it really worked
    bucket.getDirect[Int](doc.id).toFutureOption must beEqualTo(newDoc.content).await
  }

  def e6 = {
    val doc = randomDoc("testRemove")

    bucket.insert(doc).map(_.content).toFuture must beEqualTo(doc.content).await

    bucket.remove[Int](doc.id).toFuture.await

    bucket.get[Int](doc.id).toFutureOption must beNone.await
  }

  def e7 = {
    val doc = randomDoc("testCASReplace", 1)
    val result = bucket.insert(doc).toFuture.await
    result.content must beEqualTo(doc.content)

    val updateDoc = Document(result.id, Option(2), result.cas)
    val updateResult = bucket.replace(updateDoc).toFuture.await
    updateResult.content must beEqualTo(updateDoc.content)

    val failDoc: JsDocument = Document(result.id, Option(3), result.cas)

    bucket.replace(failDoc).toFuture must throwA[CASMismatchException].await
  }

}
