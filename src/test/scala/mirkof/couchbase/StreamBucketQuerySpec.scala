package mirkof.couchbase

import mirkof.couchbase.documents.Document
import mirkof.couchbase.types.CouchbaseID
import mirkof.couchbase.view.{ DesignDocument, ViewQuery, _ }
import org.specs2.concurrent._
import mirkof.couchbase.utils.StreamConversions._

import scala.util.Random

class StreamBucketQuerySpec(implicit ee: ExecutionEnv) extends CouchbaseSpec {
  def is = sequential ^ s2"""
    A StreamBucketQuery should
      query a document                                  $e1
      query a document directly                         $e2
      """

  def prepareViewQuery(): ViewQuery = {
    val viewName = "queryAllView"

    val view = View(
      name = viewName,
      map = s"""function (doc, meta) {
                |emit(doc, null)
                |}
        """.stripMargin
    )

    val designName = viewName + "Design"

    val designDocument = DesignDocument(
      name = designName,
      views = Seq(view)
    )

    bucketManager.upsertDesignDocument(designDocument).toFuture must beEqualTo(designDocument).awaitFor(longAwaitTimeout)
    ViewQuery(designName, viewName).stale(Stale.FALSE)
  }

  def randomDoc(id: String, content: Int = 0): Document[Int] = {
    val rand = Random.nextInt(Int.MaxValue)
    val cid: CouchbaseID = id + rand
    val num: Int = if (content == 0) rand else content
    Document(cid, num)
  }

  def e1 = {
    val query = prepareViewQuery()

    // create docs
    val docs: Seq[Document[Int]] = (0 to 5).map { i =>
      bucket.insert(randomDoc(s"queryDoc$i")).toFuture.await
    }

    // get result
    val result = bucket.query[Int](query).toFuture.awaitFor(longAwaitTimeout)
    result.success must beTrue

    // get rows
    val rows: Seq[Document[Int]] = result.rows.flatMapConcat(_.document).toFutureSeq.awaitFor(longAwaitTimeout)

    // validate
    rows must containAllOf(docs)
  }

  def e2 = {
    val query = prepareViewQuery()

    val values: Seq[Int] = (0 to 5).flatMap { i =>
      val doc = randomDoc(s"queryDirectDoc$i")
      bucket.insert(doc).map(_.content).toFuture must beEqualTo(doc.content).await
      doc.content
    }

    bucket.queryDirect[Int](query).toFutureSeq must containAllOf(values).awaitFor(longAwaitTimeout)
  }

}
