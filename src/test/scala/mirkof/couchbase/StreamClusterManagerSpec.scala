package mirkof.couchbase

import com.couchbase.client.java.cluster.DefaultBucketSettings
import org.specs2.concurrent._
import mirkof.couchbase.utils.StreamConversions._

import scala.util.Random

class StreamClusterManagerSpec(implicit ee: ExecutionEnv) extends CouchbaseSpec {
  // scalastyle:off public.methods.have.type
  def is = sequential ^ s2"""
    A StreamCluster should
      check if bucket 'default' exists                $e1
      open bucket 'default'                           $e2

    An StreamClusterManager should
      create a bucket                                 $e3
      check if created bucket exists                  $e4
      get bucket settings                             $e5
      update bucket settings                          $e6
      remove a bucket                                 $e7
    """

  val testBucketName = "test-bucket" + Random.nextInt(Int.MaxValue)
  val bucketSettingsBuilder = DefaultBucketSettings.builder().name(testBucketName).quota(100).enableFlush(true)
  val bucketSettings = bucketSettingsBuilder.build()

  def e1 = {
    clusterManager.hasBucket("default").toFuture must beTrue.awaitFor(longAwaitTimeout)
  }

  def e2 = {
    cluster.openBucket("default").map(_.name).toFuture must beEqualTo("default").awaitFor(longAwaitTimeout)
  }

  def e3 = {
    clusterManager.insertBucket(bucketSettings).toFuture must beEqualTo(bucketSettings).awaitFor(longAwaitTimeout)
  }

  def e4 = {
    clusterManager.hasBucket(testBucketName).toFuture must beTrue.awaitFor(longAwaitTimeout)
  }

  def e5 = {
    val settings = clusterManager.getBucket(testBucketName).toFuture.awaitFor(longAwaitTimeout)

    settings.name() === testBucketName
    settings.quota() === 100
  }

  def e6 = {
    val newSettings = bucketSettingsBuilder.quota(101).build()

    clusterManager.updateBucket(newSettings).toFuture must beEqualTo(newSettings).awaitFor(longAwaitTimeout)

    val settings = clusterManager.getBucket(testBucketName).toFuture.awaitFor(longAwaitTimeout)
    settings.name() === newSettings.name()
    settings.quota() === newSettings.quota()
  }

  def e7 = {
    clusterManager.removeBucket(testBucketName).toFuture must beTrue.awaitFor(longAwaitTimeout + longAwaitTimeout)

    clusterManager.hasBucket(testBucketName).toFuture must beFalse.awaitFor(longAwaitTimeout)
  }
}
